/*! \file  EEPROMsample.c
 *
 *  \brief Sample program to read 24FC128 serial EEPROM
 *
 *
 *  \author jjmcd
 *  \date January 11, 2016, 11:23 AM
 *
 */
#include <xc.h>
#include "EEPROMsample.h"
#include "configBits.h"

/*! main - Display three characters from the serial EEPROM */
/*! Calls the initialization routine, then calls 
 *  EEPROMshowOneCharacter() three times for three different
 *  EEPROM addresses.  Loops through the three addresses
 *  continuously.
 */
int main(void)
{
  /* Perform initialization for the application */
  EEPROMsampleInitialize();
  
  while(1)
    {
      /* Display a character from the EEPROM and its address */
      EEPROMshowOneCharacter( MESSAGE_ADDR_1 );
      /* Display another character from the EEPROM and its address */
      EEPROMshowOneCharacter( MESSAGE_ADDR_2 );
      /* And yet another */
      EEPROMshowOneCharacter( MESSAGE_ADDR_3 );      
    }
  
  /* CHALLENGE #1
   * 
   * The serial EEPROM contains a long message beginning at location
   * 0x0000 and continuing until a null (zero) byte is encountered.  Can
   * you identify what is stored at the beginning of the EEPROM?
   * 
   * CHALLENGE #2
   * 
   * At location 0x0c00 begins another, even longer message, also terminated
   * with a null character.  Can you come up with a way to display this
   * very long message on the 16 character by two line display?  Do you
   * recognize this particular bit of prose?
   * 
   * CHALLENGE #3
   * 
   * Try writing things to the EEPROM. If you don't want to disturb the
   * current contents, the area from 0x3190 to 0x3fff contains nothing,
   * as does the (smaller) area from 0x0b20 to 0x0bff.  You may want to
   * explore the header file 24FC128.h for hints.
   */

  return 0;
}
