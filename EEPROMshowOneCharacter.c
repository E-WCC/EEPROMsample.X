/*! \file  EEPROMshowOneCharacter.c
 *
 *  \brief Read a character and display it and address
 *
 *
 *  \author jjmcd
 *  \date January 11, 2016, 1:08 PM
 *
 */
#include <stdio.h>
#include"EEPROMsample.h"
#include "../include/LCD.h"
#include "../include/24FC128.h"

/*! Position of the next character on the LCD */
int nLCDposition;

/*! EEPROMshowOneCharacter - Read and display a character and its address */
/*! Reads the serial EEPROM at the address provided, displays the character
 *  at the top line of the display, and the character's address on the
 *  second line.  The address is constantly overwritten, but the character
 *  is displayed on successive locations on the top line, wrapping when
 *  the right edge of the display is reached.
 *
 * \param nCharAddress int - Address to read in serial EEPROM
 */
void EEPROMshowOneCharacter(int nCharAddress)
{
  unsigned char cLetter;
  char szWork[32];
  
  /* Read a character from the serial EEPROM.  The EEPROM has an
   * address on the I2C bus (EEPROM_ADDR) and the character has an
   * address within the serial EEPROM (nCharAddress).  */
  cLetter = read24FC128( EEPROM_ADDR, nCharAddress );
  
  /* Position the LCD cursor and display the letter */
  LCDposition( nLCDposition );
  LCDletter(cLetter);
  
  /* Move to the next position on the LCD. If the position is past
   * the right edge of the display, start again on the left */
  nLCDposition++;
  if ( nLCDposition>15 )
    nLCDposition = 0;
  
  /* Format the address we read and display it on the second line */
  sprintf( szWork, " Address 0x%04x  ", nCharAddress );
  LCDline2();
  LCDputs( szWork );
  
  /* Delay a second so the user can see it */
  Delay_ms( 1000 );
}
