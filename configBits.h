/*! \file  configBits.h
 *
 *  \brief Configuration bits for dsPIC33EV32GM002
 *
 * This file has a default set of configuration bits for the dsPIC-EL-GM
 * 
 * It should be included once, and only once, for dsPIC33EV32GM002
 * applications.
 * 
 * Selects the fast RC oscillator with PLL and turns off the 
 * watchdog timer and the deadman timer.
 *
 *  \author jjmcd
 *  \date January 11, 2016, 11:25 AM
 *
 */

#ifndef CONFIGBITS_H
#define	CONFIGBITS_H

#ifdef	__cplusplus
extern "C"
{
#endif

/*! Fast RC oscillator with PLL */
#pragma config FNOSC = FRCPLL
/*! Watchdog timer off */
#pragma config FWDTEN = OFF
/*! Deadman timer off */
#pragma config DMTEN = DISABLE



#ifdef	__cplusplus
}
#endif

#endif	/* CONFIGBITS_H */

