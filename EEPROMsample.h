/*! \file  EEPROMsample.h
 *
 *  \brief Header for EEPROMsample
 * 
 * This file contains function prototypes and constants for the sample
 * 24FC128 application for the club members to build on.
 *
 *  \author jjmcd
 *  \date January 11, 2016, 11:25 AM
 *
 */

#ifndef EEPROMSAMPLE_H
#define	EEPROMSAMPLE_H

#ifdef	__cplusplus
extern "C"
{
#endif

/* Constants for the application                                */
  
/*! I2C address of the serial EEPROM                            */
#define EEPROM_ADDR 0xaa
  
/*! Address of the first message within the serial EEPROM       */
#define MSG_1_START 0x000
/*! Address of the second message within the serial EEPROM      */
#define MSG_2_START 0xc00
  
/*! First EEPROM address to be read */
#define MESSAGE_ADDR_1 0x0c20
/*! Second EEPROM address to be read */
#define MESSAGE_ADDR_2 0x0c36
/*! Third EEPROM address to be read */
#define MESSAGE_ADDR_3 0x0cd2
  
/* Function Prototypes */
  
/*! EEPROMsampleInitialize - Perform initialization forEEPROMsample */
void EEPROMsampleInitialize(void);
/*! EEPROMsampleInitializeClock - Set the clock PLL chain for 70 MIPS */
void EEPROMsampleInitializeClock(void);
/*! EEPROMshowOneCharacter - Read and display a character and its address */
void EEPROMshowOneCharacter(int);

#ifdef	__cplusplus
}
#endif

#endif	/* EEPROMSAMPLE_H */

