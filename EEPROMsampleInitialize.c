/*! \file  EEPROMsampleInitialize.c
 *
 *  \brief Initialization for the EEPROMsample application
 *
 *  \author jjmcd
 *  \date January 11, 2016, 11:24 AM
 *
 */
#include "EEPROMsample.h"
#include "../include/LCD.h"
#include "../include/I2C.h"

/*! Position of the next character on the LCD */
extern int nLCDposition;

/*! EEPROMsampleInitialize - Perform initialization for EEPROMsample */
/*! Function calls EEPROMsampleInitializeClock() to initialize the
 *  processor clock, then initializes the LCD and displays a message.
 *  Function then initializes the I2C peripheral and the position
 *  which will be used by EEPROMshowOneCharacter().  Then delays
 *  a second to allow the user to read the display.  The 24FC128
 *  serial EEPROM uses the I2C bus, hence the need to initialize
 *  that bus.
 */
void EEPROMsampleInitialize(void)
{
  /* Set the processor clock to 70 MIPS */
  EEPROMsampleInitializeClock();
  /* Initialize the LCD and clear it */
  LCDinit();
  LCDclear();
  /* Display a message so the user knows we are working */
  LCDputs("   Initializing  ");
  /* Initialize the I2C peripheral to be used by the 24FC128 serial EEPROM */
  I2Cinit();
  /* Initialize the position of the displayed character on the display */
  /* This will be used and updated by EEPROMshowOneCharacter() */
  nLCDposition = 0;
  /* Give the user a chance to see the message */
  Delay_ms( 1000 );
  /* Start with a clear display */
  LCDclear();
}
