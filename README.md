# Sample EEPROM

`EEPROMsample.X` is a simple application to repeatedly read three bytes
for a serial EEPROM, and display those bytes and their address repeatedly.
The EEPROM contains two long messages.  Club members are challenged to
identify the first message, and to display the second.  Both messages
are far too long to fit on the LCD display, so members will need to devise
some strategy such as scrolling, displaying a word at a time, etc.

The 24FC128 serial EEPROM resides at 0x55 on the I2C bus.

---

![shield](images/Shield1BW.png)

**Shield schematic showing temperature sensor and serial EEPROM as well as LDR**

---

